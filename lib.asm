global print_string
global read_word
global exit
global print_newline
global string_length
global string_equals

section .text

exit: 
    mov rax, 60
    syscall

string_length:    
    xor rax, rax                 
  .loop:
    mov sil, byte[rdi + rax]
    test sil, sil
    jz .exit
    add rax, 1 
    jmp .loop
        				
  .exit:
    ret


print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

print_newline:
    mov rdi, 0x0A
print_char:
    push rdi
    mov rsi, rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop rdi
    ret


print_uint:
    push r10
    push r11
    mov r10, 0     ;counter
    mov r11, 10    ;divider
    mov rax, rdi
    
  .loop:
    inc r10
    mov rdx, 0
    div r11
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    
    cmp rax, 0
    je .out
    jmp .loop
    
  .out:
    mov rsi, rsp    
    mov rdi, 1
    mov rdx, r10    
    mov rax, 1      
    syscall

	  add rsp, r10
    
  .exit:
    pop r11
    pop r10
    ret

print_int:
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

string_equals:

    ;mov r8, rdi
    ;call string_length
    ;mov rdi, rsi
    ;mov r10, rax
    ;call string_length
    ;cmp rax, r10
    ;jnz .fail
    ;mov rdi, r8
    ;mov rsi, r10
    
    xor r9, r9
    xor rax, rax
    
  .loop:
    mov cl, byte[rdi+r9]
    cmp cl, byte[rsi+r9]
    jnz .fail
    
    cmp byte[rdi+r9], 0
    jz .success
    
    inc r9
    jmp .loop
  .fail:
    mov rax, 0
    ret
  .success:
    mov rax, 1
    ret
    
read_char:
    push 0
    mov rdi, 0
    mov rdx, 1
    mov rax, 0
    mov rsi, rsp
    syscall
    
    mov rax, [rsp]
    
  .exit:
    pop rdi
    ret 

read_word:
  .space:
    xor rdx, rdx
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    
    test rax, rax
    je .exit
    
    test rsi, rsi
    je .exit
    
    cmp rax, 0x20
    jz .space
    
    cmp rax, 0x9
    jz .space
    
    cmp rax, 0xA
    jz .space
    
    jmp .write
    
  .read:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    
    test rax, rax
    je .null
    
    test rsi, rsi
    je .null
    
    cmp rax, 0x20
    jz .null
    
    cmp rax, 0x9
    jz .null
    
    cmp rax, 0xA
    jz .null
    
  .write:
    mov byte[rdx + rdi], al
    inc rdx
    cmp rdx, rsi
    jz .exit
    jmp .read
  .null:
    mov byte[rdx + rdi], 0
    mov rax, rdi
    ret
  .exit:
    xor rax, rax
    xor rdx, rdx
    ret

parse_uint:
    push rbx

    xor rdx, rdx
    xor rax, rax
    xor rbx, rbx
  .loop:				
    mov bl, byte [rdi + rdx]  
    cmp bl, '0'			
    jl .exit				
    cmp bl, '9'			
    jg .exit				
    
    sub bl, '0'			
    push rdx
    mov rdx, 10
    mul rdx				
    pop rdx
    add rax, rbx			

    inc rdx				
    jmp .loop			

  .exit:
    pop rbx
    ret

parse_int:
    push rdi
    mov rax, rdi
    
    cmp byte [rax], '-'
    jz .negative
    
    cmp byte [rax], '+'
    jz .positive
    
    call parse_uint
    jmp .exit
    
  .positive:
    inc rdi
    call parse_uint
    cmp rdx, 0
    jz .exit
    
    inc rdx
    jmp .exit
    
  .negative:
    inc rdi
    call parse_uint
    cmp rdx, 0
    jz .exit
    
    neg rax
    inc rdx
    jmp .exit
    ret 
  .exit:
    pop rdi
    ret

string_copy:
    xor rax, rax
    
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    
    cmp rax, rdx
    jge .over
    
    push rax
    push r13
    
  .loop:
    cmp rax, 0
    jl .exit
    dec rax
    mov r13, [rdi+rax]
    mov [rsi+rax], r13
    cmp rax, 0
    jz .exit
    jmp .loop
    
  .exit:
    pop r13
    pop rax
    ret
  .over:
    mov rax, 0
    ret

