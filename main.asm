%include "colon.inc"
%include "words.inc"

global _start

extern print_string
extern read_word
extern exit
extern find_word
extern print_newline
extern string_length

%define STD_OUT 0x01
%define STD_ERR 0x02

section .rodata
key: db "key: ", 0
key_not_found: db "key not found", 10, 0
bad_input: db "bad request", 10, 0

section .text
_start:
    sub rsp, 256

    mov rdi, key
    mov rsi, STD_OUT
    call print_string    
    mov rdi, rsp
    mov rsi, 256

    call read_word
    cmp rax, 0      
    je .print_bad_input

    mov rdi, rax
    mov rsi, LAST_POINTER 
    call find_word
    cmp rax, 0
    je .print_word_not_found

    mov rdi, rax 
    add rdi, 8      
    push rdi
    call string_length 
    pop rdi
    add rdi, rax  
    inc rdi     
    mov rsi, STD_OUT
    call print_string
    call print_newline
    add rsp, 256
    call exit

.print_bad_input:
    mov rdi, bad_input
    mov rsi, STD_ERR
    call print_string
    add rsp, 256
    call exit

.print_word_not_found:
    mov rdi, key_not_found
    mov rsi, STD_ERR
    call print_string
    add rsp, 256
    call exit
