global find_word

extern string_equals

section .text
find_word:
    cmp rsi, 0 
    je .not_found
    add rsi, 8
    push rdi   
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1   
    jz  .get_element
    mov rsi, [rsi - 8]
    jmp find_word

.get_element:
    sub rsi, 8  
    mov rax, rsi
    ret

.not_found:
    mov rax, 0 
    ret
